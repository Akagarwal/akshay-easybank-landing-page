// EventListener in HAMBURGER-ICON:

const hamBurger = document.querySelector('#hamburger-icon').addEventListener('click', pop_Up_Menu);
function pop_Up_Menu() {
    const displayMenu = document.querySelector('.nav');
    displayMenu.style.display = 'block';

    const hamMenu = document.querySelector('#hamburger-icon');
    hamMenu.style.display = 'none';

    const closeMenu = document.querySelector('#close-icon');
    closeMenu.style.display = 'block';
};

const closeTheMenu = document.querySelector('#close-icon').addEventListener('click', popUpMenu_close);
function popUpMenu_close() {
    const displayMenu = document.querySelector('.nav');
    displayMenu.style.display = 'none';

    const closeMenu = document.querySelector('#close-icon');
    closeMenu.style.display = 'none';

    const hamMenuShow = document.querySelector('#hamburger-icon');
    hamMenuShow.style.display = 'block';
}